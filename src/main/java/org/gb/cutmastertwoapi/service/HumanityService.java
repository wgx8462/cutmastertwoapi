package org.gb.cutmastertwoapi.service;

import lombok.RequiredArgsConstructor;
import org.gb.cutmastertwoapi.entity.Friend;
import org.gb.cutmastertwoapi.entity.Humanity;
import org.gb.cutmastertwoapi.model.humanity.HumanityCreateRequest;
import org.gb.cutmastertwoapi.model.humanity.HumanityItem;
import org.gb.cutmastertwoapi.model.humanity.HumanityItemNameChangeRequest;
import org.gb.cutmastertwoapi.model.humanity.HumanityResponse;
import org.gb.cutmastertwoapi.repository.HumanityRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HumanityService {
    private final HumanityRepository humanityRepository;

    public void setHumanity(Friend friend, HumanityCreateRequest request) {
        Humanity addData = new Humanity();
        addData.setFriend(friend);
        addData.setGiveAndTake(request.getGiveAndTake());
        addData.setItemName(request.getItemName());
        addData.setPrice(request.getPrice());
        addData.setDateGNT(LocalDate.now());

        humanityRepository.save(addData);

    }

    public List<HumanityItem> getHumanitys() {
        List<Humanity> originList = humanityRepository.findAll();
        List<HumanityItem> result = new LinkedList<>();

        for (Humanity humanity : originList) {
            HumanityItem addItem = new HumanityItem();
            addItem.setFriendId(humanity.getFriend().getId());
            addItem.setFriendName(humanity.getFriend().getName());
            addItem.setId(humanity.getId());
            addItem.setGiveAndTake(humanity.getGiveAndTake().getName());
            addItem.setItemName(humanity.getItemName());
            result.add(addItem);
        }
        return result;
    }

    public HumanityResponse getHumanity(long id) {
        Humanity originData = humanityRepository.findById(id).orElseThrow();
        HumanityResponse response = new HumanityResponse();

        response.setFriendId(originData.getFriend().getId());
        response.setFriendName(originData.getFriend().getName());
        response.setId(originData.getId());
        response.setGiveAndTake(originData.getGiveAndTake());
        response.setItemName(originData.getItemName());
        response.setPrice(originData.getPrice());
        response.setDateGNT(originData.getDateGNT());
        return response;
    }

    public void putItemName(long id, HumanityItemNameChangeRequest request) {
        Humanity originData = humanityRepository.findById(id).orElseThrow();

        originData.setItemName(request.getItemName());

        humanityRepository.save(originData);
    }

    public void delHumanity(long id) {
        humanityRepository.deleteById(id);
    }
}
