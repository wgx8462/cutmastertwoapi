package org.gb.cutmastertwoapi.service;


import lombok.RequiredArgsConstructor;
import org.gb.cutmastertwoapi.entity.Friend;
import org.gb.cutmastertwoapi.model.friend.*;
import org.gb.cutmastertwoapi.repository.FriendRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public Friend getData(long id) {
        return friendRepository.findById(id).orElseThrow();
    }

    public void setFriend(FriendCreateRequest request) {
        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setCutWhether(false);

        friendRepository.save(addData);
    }

    public List<FriendItem> getFriends() {
        List<Friend> originList = friendRepository.findAll();
        List<FriendItem> result = new LinkedList<>();

        for (Friend friend : originList) {
            FriendItem addItem = new FriendItem();
            addItem.setId(friend.getId());
            addItem.setName(friend.getName());
            addItem.setPhoneNumber(friend.getPhoneNumber());
            result.add(addItem);
        }
        return result;
    }

    public FriendResponse getFriend(long id) {
        Friend originData = friendRepository.findById(id).orElseThrow();
        FriendResponse response = new FriendResponse();

        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setBirthDay(originData.getBirthDay());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());
        response.setCutWhether(originData.getCutWhether());
        response.setDateCut(originData.getDateCut());
        response.setCutReason(originData.getCutReason());

        return response;
    }

    public void putChangeName(long id, FriendNameChangeRequest request) {
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setName(request.getName());

        friendRepository.save(originData);
    }

    public void putFriendCut(long id, FriendCutChangeRequest request) {
        Friend originData = friendRepository.findById(id).orElseThrow();

        originData.setCutWhether(true);
        originData.setDateCut(LocalDate.now());
        originData.setCutReason(request.getCutReason());
        friendRepository.save(originData);
    }

    public void putFriendCutCancel(long id) {
        Friend originData = friendRepository.findById(id).orElseThrow();

        originData.setCutWhether(false);
        originData.setDateCut(null);
        originData.setCutReason(null);
        friendRepository.save(originData);
    }
}
