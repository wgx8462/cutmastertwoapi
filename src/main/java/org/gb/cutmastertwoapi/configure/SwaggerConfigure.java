package org.gb.cutmastertwoapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "cut master App",
                description = "cut master app api명세",
                version = "vi"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfigure {

    @Bean
    public GroupedOpenApi healthOpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("친구정보 API v1")
                .pathsToMatch(paths)
                .build();
    }

}