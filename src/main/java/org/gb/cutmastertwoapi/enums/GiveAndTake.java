package org.gb.cutmastertwoapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GiveAndTake {
    GIVE("주다"),
    TAKE("받다");

    private final String name;
}
