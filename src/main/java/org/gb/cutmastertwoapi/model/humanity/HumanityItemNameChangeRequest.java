package org.gb.cutmastertwoapi.model.humanity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HumanityItemNameChangeRequest {
    private String itemName;
}
