package org.gb.cutmastertwoapi.model.humanity;

import lombok.Getter;
import lombok.Setter;
import org.gb.cutmastertwoapi.enums.GiveAndTake;

@Getter
@Setter
public class HumanityItem {
    private Long friendId;
    private String friendName;
    private Long id;
    private String giveAndTake;
    private String itemName;
}
