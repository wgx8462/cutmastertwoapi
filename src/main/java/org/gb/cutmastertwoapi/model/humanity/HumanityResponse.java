package org.gb.cutmastertwoapi.model.humanity;

import lombok.Getter;
import lombok.Setter;
import org.gb.cutmastertwoapi.enums.GiveAndTake;

import java.time.LocalDate;

@Getter
@Setter
public class HumanityResponse {
    private Long friendId;
    private String friendName;
    private Long id;
    private GiveAndTake giveAndTake;
    private String itemName;
    private Double price;
    private LocalDate dateGNT;
}
