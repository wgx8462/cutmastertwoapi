package org.gb.cutmastertwoapi.model.humanity;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import org.gb.cutmastertwoapi.enums.GiveAndTake;

import java.time.LocalDate;

@Getter
@Setter
public class HumanityCreateRequest {
    @Enumerated(value = EnumType.STRING)
    private GiveAndTake giveAndTake;
    private String itemName;
    private Double price;
}
