package org.gb.cutmastertwoapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendNameChangeRequest {
    private String name;
}
