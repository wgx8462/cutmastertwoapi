package org.gb.cutmastertwoapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendResponse {
    private Long id;
    private String name;
    private LocalDate birthDay;
    private String phoneNumber;
    private String etcMemo;
    private Boolean cutWhether;
    private LocalDate dateCut;
    private String cutReason;
}
