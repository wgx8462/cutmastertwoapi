package org.gb.cutmastertwoapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendItem {
    private Long id;
    private String name;
    private String phoneNumber;
}
