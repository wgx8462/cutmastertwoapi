package org.gb.cutmastertwoapi.controller;

import lombok.RequiredArgsConstructor;
import org.gb.cutmastertwoapi.entity.Friend;
import org.gb.cutmastertwoapi.model.humanity.HumanityCreateRequest;
import org.gb.cutmastertwoapi.model.humanity.HumanityItem;
import org.gb.cutmastertwoapi.model.humanity.HumanityItemNameChangeRequest;
import org.gb.cutmastertwoapi.model.humanity.HumanityResponse;
import org.gb.cutmastertwoapi.service.FriendService;
import org.gb.cutmastertwoapi.service.HumanityService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/humanity")
public class HumanityController {
    private final FriendService friendService;
    private final HumanityService humanityService;

    @PostMapping("/new/friend-id/{friendId}")
    public String setHumanity(@PathVariable long friendId, @RequestBody HumanityCreateRequest request) {
        Friend friend = friendService.getData(friendId);
        humanityService.setHumanity(friend, request);

        return "등록 완료";
    }

    @GetMapping("/all")
    public List<HumanityItem> getHumanitys() {
        return humanityService.getHumanitys();
    }

    @GetMapping("/detail/humanity-id/{humanityId}")
    public HumanityResponse getHumanity(@PathVariable long humanityId) {
        return humanityService.getHumanity(humanityId);
    }

    @PutMapping("/item-name/humanity-id/{humanityId}")
    public String putItemName(@PathVariable long humanityId, @RequestBody HumanityItemNameChangeRequest request) {
        humanityService.putItemName(humanityId, request);

        return "수정 완료";
    }

    @DeleteMapping("/humanity-id/{humanityId}")
    public String delHumanity(@PathVariable long humanityId) {
        humanityService.delHumanity(humanityId);

        return "삭제 완료";
    }
}
