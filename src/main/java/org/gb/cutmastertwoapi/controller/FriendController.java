package org.gb.cutmastertwoapi.controller;

import lombok.RequiredArgsConstructor;
import org.gb.cutmastertwoapi.model.friend.*;
import org.gb.cutmastertwoapi.service.FriendService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {
    private final FriendService friendService;

    @PostMapping("/join")
    public String setFriend(@RequestBody FriendCreateRequest request) {
        friendService.setFriend(request);

        return "등록 완료";
    }

    @GetMapping("/all")
    public List<FriendItem> getFriends() {
        return friendService.getFriends();
    }

    @GetMapping("detail/{id}")
    public FriendResponse getFriend(@PathVariable long id) {
        return friendService.getFriend(id);
    }

    @PutMapping("/change-name/{id}")
    public String putChangeName(@PathVariable long id, @RequestBody FriendNameChangeRequest request) {
        friendService.putChangeName(id, request);

        return "이름 수정 완료";
    }

    @PutMapping("/friend-cut/{id}")
    public String putFriendCut(@PathVariable long id, @RequestBody FriendCutChangeRequest request) {
        friendService.putFriendCut(id, request);

        return "OK";
    }

    @PutMapping("friend-cut-cancel/{id}")
    public String putFriendCutCancel(@PathVariable long id) {
        friendService.putFriendCutCancel(id);

        return "OK";
    }
}
