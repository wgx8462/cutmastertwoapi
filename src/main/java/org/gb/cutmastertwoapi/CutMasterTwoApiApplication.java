package org.gb.cutmastertwoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CutMasterTwoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CutMasterTwoApiApplication.class, args);
	}

}
