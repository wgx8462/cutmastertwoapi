package org.gb.cutmastertwoapi.repository;

import org.gb.cutmastertwoapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend, Long> {
}
