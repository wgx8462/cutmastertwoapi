package org.gb.cutmastertwoapi.repository;

import org.gb.cutmastertwoapi.entity.Humanity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HumanityRepository extends JpaRepository<Humanity, Long> {
}
